# Copyright 2022 Ciberth

# TODOS
# Parameters (refreshrate, snapshotrate, rawdata path, custom csv delta's)
# timeline diffs visualise over time
# fix filename to include seconds or writing over & over for each min?

# create rawdata folder
$path = ".\rawdata"
if(!(test-path $path)) {
    write-host ".\rawdata folder not present, creating one now..."
    New-Item -ItemType Directory -Force -Path $path
}

function snapshot {
    $(Get-CimInstance -Class Win32_Process | select ProcessName, ProcessId, ParentProcessId, CreationDate, Path) | Export-Csv -Path "$path/$(get-date -Format "dd-MM_HH-mm").csv"
}

function main {
    # read oldest csv
    $oldest_file_name = (ls $path | sort name).name[0]
    $baseline = Import-Csv -Path "$path\\$oldest_file_name"
    $newest_file_name = (ls $path | sort name).name[-1]
    $last_snapshot = Import-Csv -Path "$path\\$newest_file_name"
    compare-object $baseline $last_snapshot | select -expandproperty inputobject | ft
}

# run snapshot once before infinite loop
snapshot
sleep 5

while(1) {
    main
    snapshot
    sleep 5
    clear-host
}
