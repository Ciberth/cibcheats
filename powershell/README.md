# Interesting cheatsheets

## General 

```powershell

# Single line comment

<#
multi
line
comments
#>


```


## Parameters

```powershell

Param (
  [string] $LogName,
  [string] $Computername = $env:computername,
  [string] $Path,
  [int32] $Newest = 500
)

```


## Hashtable

```powershell

# array is $arr = @()
# hash is $hash = @{}

## example

$params = @{
  Classname = 'win32_logicaldisk'
  Filter = "deviceid='c:'"
  Verbose = $True
}

Get-CimInstance @params

```

## Define custom (new) properties with a custom hashtable

> @{ Name="Size"; Expression = {$_.Length} }


> dir -file | Select-Object Name, LastWriteTime, @{Name="mySize"; Expression={$_.Length}}


## PSObject

```powershell

$files = dir -file
$now = Get-Date

foreach ($file in $files) {
  $hash = @{
     myName = $file.name
     mySize = $file.length
     myAge = $now - $file.lastwritetime
  }
  New-Object psobject -Property $hash
}

## OR

$files = dir -file
$now = Get-Date

foreach ($file in $files) {
  [pscustomobject] @{
     myName = $file.name
     mySize = $file.length
     myAge = $now - $file.lastwritetime
  }
}

```


## Example combining [math], custom hashtable, and casting to [int]

```powershell

Get-CimInstance Win32_OperatingSystem | 
Select PSComputername,@{Name="TotalMemGB";Expression={$_.totalvisiblememorysize/1MB -as [int]}},
@{Name="FreeMemGB";Expression={ [math]::Round(($_.freephysicalmemory/1Mb),4)}},
@{Name="PctFreeMem";Expression = {[math]::Round(($_.freephysicalmemory/$_.totalvisiblememorysize)*100,2)}}

```

## Example for custom password creation

```powershell

#make a password
$alpha = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,'
$alpha += $alpha.ToUpper()
$num = 0..9
$char = '!,),@,(,*,/,$,&,^,%,[,],>,<'
$arr = @()
$arr+= $alpha.split(',') | Get-Random -count 10
#make the first and last elements upper case
$arr[0] = $arr[0].toupper()
#$arr[-1] = $arr[-1].toupper()
#$num is already an array
$arr += $num | Get-Random -count 2
#get 3 random characters
$arr += $char.split(',') | Get-Random -count 5
#randomize the array and join as a string 
($arr | Get-Random -Count $arr.count) -join ""

```

## Last edited files

```powershell
$rootPath = "C:\Users\User\AppData"
$daysSinceNow = "-0.5"

Get-ChildItem -Path $rootPath -Recurse -ErrorAction SilentlyContinue -File -Force `
    | Where-Object {($_.LastWriteTime -gt ([DateTime]::Now.Adddays($daysSinceNow))) -and ($_.PSIsContainer -eq $False) } `
        | Select-Object Name, LastWriteTime, @{l='Path';e={$_.PSPath.substring(38)}} `
            | Sort-Object -pro LastWriteTime -Descending 


# .\last_edited_files.ps1 | ? {($_ -notmatch 'Opera') -and ($_ -notmatch 'Google') -and ($_ -notmatch 'Microsoft') }

```