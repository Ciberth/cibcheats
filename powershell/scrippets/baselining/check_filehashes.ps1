# CREATE BASELINE WITH
# $(Get-ChildItem -Path . -Recurse | Get-FileHash) | Export-Csv -Path "./base-filehashes.csv"
# run with
# .\paramFileHashCheck.ps1 -folder . -file .\base-filehashes.csv

param ($folder=".", $file="base-filehashes.csv")

Write-Output "[+] Fetching file hashes from pwd (recursively)..."

$file_hashes_live = Get-ChildItem -Path $folder -Recurse | Get-FileHash 

Write-Output "[+] Reading base file..."

$file_hashes_base = Import-Csv -Path "./$file"
[String[]]$file_hashes_base_arr = @();
[String[]]$file_hashes_live_arr = @();


foreach ($hash in $file_hashes_base) {
    # Fill 2 arrays (one for base & one for live) to compare 2 arrays later without losing objects
    $file_hashes_base_arr += $hash.Hash
}

foreach ($hash in $file_hashes_live) {
    $file_hashes_live_arr += $hash.Hash
}

Write-Output "[+] Comparing base to file ..."

$only_live = [String[]][Linq.Enumerable]::Except($file_hashes_live_arr, $file_hashes_base_arr)
$only_file = [String[]][Linq.Enumerable]::Except($file_hashes_base_arr, $file_hashes_live_arr)

#$matches = [String[]][Linq.Enumerable]::Intersect($file_hashes_base_arr, $file_hashes_live_arr)

Write-Output "[+] Hashes only found live, not in base..."

foreach ($hl in $only_live) {
    foreach ($hb in $file_hashes_live) {
        if ($hb.Hash -like $hl) {
            Write-Output "`t$($hb.Path | Out-String -Stream)"
        }
    }    
}

Write-Output "[+] Hashes only found in base, not live..."

foreach ($hl in $only_file) {
    foreach ($hb in $file_hashes_base) {
        if ($hb.Hash -like $hl) {
            Write-Output "`t$($hb.Path | Out-String -Stream)"
        }
    }    
}
