$rootPath = "C:\Users\User\AppData"
$daysSinceNow = "-0.5"

Get-ChildItem -Path $rootPath -Recurse -ErrorAction SilentlyContinue -File -Force `
    | Where-Object {($_.LastWriteTime -gt ([DateTime]::Now.Adddays($daysSinceNow))) -and ($_.PSIsContainer -eq $False) } `
        | Select-Object Name, LastWriteTime, @{l='Path';e={$_.PSPath.substring(38)}} `
            | Sort-Object -pro LastWriteTime -Descending 


# grep -v equivalent in PowerShell
# .\last_edited_files.ps1 | ? {($_ -notmatch 'Opera') -and ($_ -notmatch 'Google') -and ($_ -notmatch 'Microsoft') }

