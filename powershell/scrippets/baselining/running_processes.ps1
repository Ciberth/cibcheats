# CREATE BASELINE FOR RUNNING PROCESSES
# # Running Processes
# $(Get-Process | Select-Object -Property ProcessName, Path -Unique) | Export-Csv -Path "./base-runningprocesses.csv"

Write-Output "[+] Fetching live system metrics, running processes..."

$live_processes = $(Get-Process | Select-Object -Property Path -Unique)
[String[]]$live_processes_arr = @();

Write-Output "[+] Reading base file..."

$file_processes = Import-Csv -Path "./base-runningprocesses.csv"
[String[]]$file_processes_arr = @();


foreach ($p in $live_processes) {
    # Fill 2 arrays (one for base & one for live) to compare 2 arrays later without losing objects
    $live_processes_arr += $p.Path
}

foreach ($p in $file_processes) {
    $file_processes_arr += $p.Path
}

#$file_processes_arr | get-unique -AsString

# Make array that only has unique processnames
[String[]]$live_processes_arr_unique = $($live_processes_arr | Sort-Object -Property "Path" | Get-Unique -AsString)
[String[]]$file_processes_arr_unique = $($file_processes_arr | Sort-Object -Property "Path" | Get-Unique -AsString)



# Faster by using Linq
$only_live = [String[]][Linq.Enumerable]::Except($live_processes_arr_unique, $file_processes_arr_unique)
$only_file = [String[]][Linq.Enumerable]::Except($file_processes_arr_unique, $live_processes_arr_unique)

# $matches= [String[]][Linq.Enumerable]::Intersect($file_processes_arr, $live_processes_arr)


Write-Output "[!] Processes running live that are not in base..."
#$b = (($only_live  -split "=") | sls -Pattern "C:\\*" ) -replace ".$"

foreach ($el in $only_live) {
    foreach ($p in $live_processes) {
        if ($p.Path -like $el) {
            Write-Output "`t$el"
        }
    }    
}

Write-Output "[!] Processes in base that are not running live..."

foreach ($el in $only_file) {
    foreach ($p in $file_processes) {
        if ($p.Path -like $el) {
            Write-Output "`t$el"
        }
    }    
}