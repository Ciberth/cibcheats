# Stuff for in profile - quality of life things

## Sources



Profile locations: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-7.1 

### For Current User, Current Host

> code $PROFILE


## Map CTRL + A & CTRL + E 

> Set-PSReadLineKeyHandler -Key ctrl+a -Function BeginningOfLine

> Set-PSReadLineKeyHandler -Key ctrl+e -Function EndOfLine


## Custom prompt notes

```powershell
notepad $PSHOME\profile.ps1
## or in $PROFILE for current user current host

## example 1

function prompt {"PS: $(get-date)>"}

## example 2

function prompt
{
    Write-Host ("PS " + $(get-date) +">") -nonewline -foregroundcolor Blue
    return " "
}

## example 3

function prompt
{
    Write-Host ("`n`n$(get-date) | PS [$Env:userdomain\\$Env:username@$Env:computername] $($PWD.ProviderPath)`n> ") -nonewline -foregroundcolor Blue
    return " "
}​​

# Set-ExecutionPolicy RemoteSigned
```