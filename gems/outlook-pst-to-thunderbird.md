# Migration outlook to thunderbird

## Interesting tool 


Source: https://gist.github.com/cubapp/fdccb5a94019294f4e673238c74c59a6

```
Terminal:
sudo apt install pst-utils
mkdir migrated-emails
readpst -o ./migrated-emails -M -u -w -e -b outlook.pst

Thunderbird:
1. install "ImportExportTools NG" add on
2. make new folder "MigratedEmails" in Thunderbird - preferably in "Lofal Folders"
3. right click on MigratedEmails folder -> ImportExportTools NG 
                                            -> import all messages from a directory 
                                                -> also from its subdirectories 
4. choose "migrated-emails" folder
5. wait and you'll see emails apearing in the MigratedEmails folder. 

# thanks to 
# https://www.exratione.com/2013/11/importing-email-from-outlook-on-windows-to-thunderbird-on-ubuntu/
```