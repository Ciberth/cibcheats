# Vim notes

## Sample .vimrc file

Note: make the undodir: ``mkdir ~/.vim/undodir -p`` and ``:PlugInstall`` 

```
syntax on

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch

highlight ColorColumn ctermbg=0 guibg=lightgrey

call plug#begin('~/.vim/plugged')
Plug 'gruvbox-community/gruvbox'
Plug 'vim-utils/vim-man'
call plug#end()

colorscheme gruvbox
set background=dark

```



## Certain Keybindings

| Keybinding    | Action             | 
| ------------- |:------------------:| 
| CTRL + A      | Increment a number | 
| CTRL + X      | Decrement a number |  


## One-liners

_Use case:_ Imagine opening a config file, making changes only to find out that you forgot to open vim as sudo and the file is read-only. Another handy tool is **sudoedit**

> :w !sudo tee %

_Explenation:_ Run write command with shell (!) command: sudo tee to current file (%)




## Macro notes 

In command mode:

```bash
q       # Start recording a macro
a       # to what register/keybind
10@a    # perform macro 10 times

# Example
vim test.txt
# insertion mode leave first line blank and on line 2 type: 
# 1 placeholder 100
# go to first line, first column
q           # start recording 
a           # register a
j           # go one line down
^           # go to the start of the line 
ctrl + a    # increment first number
$           # go to end of the line
ctrl + x    # decrement last number
^           # go to start of the line
k           # go back up one line
q           # end recording

50@a        # perform macro 50 times

```


## Edit binary file with xxd & vim

```bash
# Example binary
# 00002000  01 00 02 00 00 48 65 6c  6c 6f 20 57 6f 72 6c 64  |.....Hello World|
# change 65 -> 61 to end up with "Hallo World"

xxd binary | vim -
:%!xxd -r > new-binary 

```
