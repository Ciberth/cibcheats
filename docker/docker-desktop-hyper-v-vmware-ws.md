# Docker Desktop on Windows

## TL;DR

Docker desktop uses Hyper-V to run containers inside a virtual machine (behind the scenes). With the last versions of Windows 10 and VMware workstation both can run together (so docker desktop + VMware workstation) but nested virtualization won't work in VMware Workstation.

## Explained

This post summarizes some confusions about running docker on Windows 10. If you want to install Docker Desktop on a Windows 10 machine there are some requirements [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/). 

One of these requirements is running the Hyper-V hypervisor feature. With the help of a hypervisor a host machine can create, run and manage virtual machines. In fact, if you use Docker Desktop you are actually **using a virtual machine** behind the scene. The same is true about WSL2 [https://docs.microsoft.com/en-us/windows/wsl/wsl2-faq](https://docs.microsoft.com/en-us/windows/wsl/wsl2-faq). WSL is a way to have a Linux distribution running on your Windows machine allowing for a Linux/bash experience. WSL version 1 does not use virtualization but rather emulation. Both aim to offer a Linux experience but, in another way, each having weak and strong points.

For most users that want to run Docker Desktop this is perfectly fine and running Hyper-V comes with no real downsides. If you are however using other Hypervisors (such as VirtualBox or VMware Workstation) things might get tricky especially with VMware. Luckily, a while back Microsoft and VMware decided to play nice and made it possible to run both hypervisors together (https://blogs.vmware.com/workstation/2020/05/vmware-workstation-now-supports-hyper-v-mode.html)[https://blogs.vmware.com/workstation/2020/05/vmware-workstation-now-supports-hyper-v-mode.html] Note that you at least need VMware workstation 15.5.5 (or higher) and Windows 10 20H1 build 19041.264 (or higher). 

## Attention!

There is however one important caveat. While it is now true that you can run a Hyper-V virtual machine and a VMware Workstation virtual machine at the same time there is one feature that is unavailable in such a setup: nested virtualization on VMware Workstation.
If the only Hypervisor is VMware Workstation (so in other words Hyper-V is disabled) it is possible to create a virtual machine inside a virtual machine (for example a Windows 10 with an ESXi virtual machine running a Debian virtual machine). **This feature is currently not possible if Hyper-V is enabled (and in other words Docker Desktop).**

## Some tips

### To turn Hyper-V on/off

> bcdedit /set hypervisorlaunchtype off

followed by a reboot, use _auto_ for hyper-v/wsl/docker/sandbox
