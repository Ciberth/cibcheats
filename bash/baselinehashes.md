# Simple manual check

Key idea also translated in audits: https://www.tenable.com/audits/items/DISA_STIG_AIX_6.1_v1r14.audit:40d2367a2cf00f1ea3819826e3fc5506 



```sh
#!/bin/bash

# Fastest

# sudo find /etc -type f -print0 2>/dev/null | xargs -0 md5sum 2>/dev/null

# Good src for diff between -exec vs xargs and {} \; vs {} \+
# https://www.everythingcli.org/find-exec-vs-find-xargs/  

# find -type f -exec md5sum {} \; > hashes
# ./check.sh 2>/dev/null | grep "FAILED"

# OR https://unix.stackexchange.com/questions/340255/how-to-compare-a-list-of-hashes-line-by-line-against-another-list-of-hashes

file="binhashes"

while read -r line; do
    echo $line | md5sum -c
done <$file
```




# POC

## Compare

```sh
#!/bin/bash

dir1=202204071000
dir2=202204072840

for file in $(find $dir1 -type f | awk -F'/' '{print $NF}'); do
        echo "DIFF in $file"
        join -v 2 <( sort "./$dir1/$file" ) <( sort "./$dir2/$file" )
        echo "---"
done
```


## Collect

```sh
#!/bin/bash

# find -type f -exec md5sum {} \; > hashes
# ./check.sh 2>/dev/null | grep "FAILED"
# TODO: maybe better to retrieve everything and remove /tmp, /dev, and others (?)
# Attention: might be usefull to monitor /dev/shm, inotifywait? 
rootfolders=("/boot" "/etc" "/home" "/media" "/mnt" "/opt" "/root" "/srv" "/usr" "/var")

foldername=$(date +%Y%m%d%M%S)
mkdir -p  "$foldername"

for rf in ${rootfolders[@]}
do
        echo "Calculating hashes for all files in $rf"
        find $rf -type f -exec md5sum {} \; > $foldername/$rf-hashes
done
```
