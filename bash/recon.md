# Linux/Bash Recon automated script

# Once you know, you know
```sh
find / -type f -printf "%T+ %p\n" 
find /etc -type f -printf "%T+ %p\n" | grep -v "0000000000"
```

# To explore: FIM

https://github.com/Achiefs/fim 

## Single ones

https://book.hacktricks.xyz/generic-methodologies-and-resources/basic-forensic-methodology/linux-forensics 

```bash
find /directory -type f -mtime -1 -print #Find modified files during the last minute in the directory

#RedHat
rpm -Va

#Debian
dpkg --verify
debsums | grep -v "OK$" #apt install debsums

## Package Manager info

#Debian
cat /var/lib/dpkg/status | grep -E "Package:|Status:"
cat /var/log/dpkg.log | grep installed
#RedHat
rpm -qa --root=/ mntpath/var/lib/rpm

# do not forget ls /opt /usr/local for binaries installed and compiled on the system

```

(WIP)

(inspired bij linpeas)


## Read (as root) all users and $PATH of all users

```bash
#!/bin/bash

for user in $(cat /etc/passwd | awk -F: '{print $1}'); do
    echo "FOR USER $user"

    canlogin=$(cat /etc/passwd | grep "^$user:" | sed 's|.*/||')
    if [ $canlogin == "bash" ];
    then
        su - $user -c '. ~/.profile; printf "%s\n" "$PATH"'
    fi
done
```
## Enumerating /etc/defaults

```bash
#!/bin/bash

printf "\e[1;31m[!] Enumerating /etc/defaults without empty lines and lines that start with # \e[0m\n"


for file in $(find /etc/default -type f 2>/dev/null)
do
    printf "\e[1;32m[+] Reading file: $file\e[0m\n"
    grep -v '^\s*$\|^\s*\#' $file
done
```


# WIP

## main.sh

```sh
#!/bin/bash


for file in $(find . -type f 2>/dev/null | grep "enum*")
do
    printf "\e[1;36m[!] - Executing script - $file \e[0m\n"
    bash $file
done
```

## Enum-users.sh

```sh
#!/bin/bash

printf "\e[1;31m[!] Enumerating users \e[0m\n"


printf "\e[1;32m[+] Currently logged in users using w \e[0m\n"

w

printf "\e[1;32m[+] List of all groups (from /etc/group) and their members \e[0m\n"
for group in $(cat /etc/group | awk -F: '{print $1}'); do
    printf "User(s) of "
    getent group $group | cut -d: -f1,4
done


printf "\e[1;32m[+] Enumerating users from /etc/passwd file and displaying their groups \e[0m\n"
for user in $(cat /etc/passwd | awk -F: '{print $1}'); do
    printf "Group(s) of "
    groups $user
done
```

## enum-sudoers.sh

```sh
#!/bin/bash

printf "\e[1;31m[!] Enumerating /etc/sudoers file without empty lines and lines that start with # \e[0m\n"

grep -v '^\s*$\|^\s*\#' /etc/sudoers


printf "\e[1;31m[!] Enumerating /etc/sudoers.d/ and files without empty lines and lines that start with # \e[0m\n"

for file in $(find /etc/sudoers.d/ -type f 2>/dev/null)
do
    printf "\e[1;32m[+] Reading file: $file\e[0m\n"
    grep -v '^\s*$\|^\s*\#' $file
done
```

## enum-path.sh

```sh
#!/bin/bash

printf "\e[1;31m[!] Do not forget to run "alias" for current user and/or root !! \e[0m\n"

printf "\e[1;31m[!] Enumerating PATH info for all users # \e[0m\n"

for user in $(cat /etc/passwd | awk -F: '{print $1}'); do

    printf "\e[1;32m[+] FOR USER $user\e[0m\n"

    canlogin=$(cat /etc/passwd | grep "^$user:" | sed 's|.*/||')
    if [ $canlogin == "bash" ];
    then
        su - $user -c '. ~/.profile; printf "%s\n" "$PATH"'
    fi
done
```

## enum-etcdefaults.sh

```sh
#!/bin/bash

printf "\e[1;31m[!] Enumerating /etc/defaults without empty lines and lines that start with # \e[0m\n"


for file in $(find /etc/default -type f 2>/dev/null)
do
    printf "\e[1;32m[+] Reading file: $file\e[0m\n"
    grep -v '^\s*$\|^\s*\#' $file
done
```

## TODOS

- enumerate users
    - to what group does each user belong to
    - list groups
    - list ssh keys if any for all users
- enumerate services (systemd)
    - show running ones 
    - show failed ones
    - show enabled ones (show running ones that are not enabled and show failed ones that are not enabled)
- enumerate open/listening ports
- explore /opt & /srv folders for manually installed software
- enumerate installed packages
- enumerate kernel and distribution versions
- verify alias that might cause troubles
- verify mounts / filesystem 
- enumerate cron jobs / at jobs
 
